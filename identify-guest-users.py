#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os
import re

def check_guest(user, period):
    contribution_events = user.events.list(iterator=True)
    print("%s has been active in the last %s days, checking events." % (user.username, str(period)))
    for event in contribution_events:
        event_dt = datetime.strptime(event.created_at, '%Y-%m-%dT%H:%M:%S.%f%z')
        delta_event = datetime.now(tz) - event_dt
        if delta_event.days > period:
            return True
        for action in ["pushed", "accepted", "closed", "approved", "reopened", "destroyed", "merged", "imported"]:
            if action in event.action_name:
                return False
        if event.action_name == "opened" and event.target_type == "MergeRequest":
                return False
    # no events found in period
    #print("Parsed all events for %s and did not find non-guest actions." % user.username)
    return True

def has_memberships(user):
    memberships = user.memberships.list(iterator=True)
    if memberships:
        return True
    return False

def highest_role(gl, user):
    memberships = user.memberships.list(iterator=True)
    highest_access_level = 0
    project_maintainer_count = 0
    project_owner_count = 0
    group_maintainer_count = 0
    group_owner_count = 0
    personal_project_count = 0
    for membership in memberships:
        # higher than dev rights, let's check if it's a personal project
        if membership.access_level > 30:
            # only consider project access level as valid if it is not a personal project
            if membership.source_type == "Project":
                project = gl.projects.get(membership.source_id)
                # not a private project. Check if access level is higher than known before and track the project
                if project.namespace["kind"] != "user":
                    if membership.access_level > highest_access_level:
                        highest_access_level = membership.access_level
                    if membership.access_level == 40:
                        project_maintainer_count += 1
                    elif membership.access_level == 50:
                        project_owner_count += 1
                # private project, just track that this exists. We may want to delete private projects for some user that should stay guests
                else:
                    personal_project_count += 1
            # group membership is always valid and has to be checked if it is higher than known
            else:
                if membership.access_level > highest_access_level:
                    highest_access_level = membership.access_level
                if membership.access_level == 40:
                    group_maintainer_count += 1
                elif membership.access_level == 50:
                    group_owner_count += 1
        # just a dev or reporter, who may be transformed to guests more readily than maintainers or owners
        else:
            if membership.access_level > highest_access_level:
                highest_access_level = membership.access_level
    return {"highest_access_level_non_personal":highest_access_level, "project_maintainer_count_non_personal": project_maintainer_count, "project_owner_count_non_personal":project_owner_count, "group_maintainer_count":group_maintainer_count, "group_owner_count": group_owner_count, "personal_project_count":str(personal_project_count)}

parser = argparse.ArgumentParser(description='Identify potential Guest users on GitLab self-managed')
parser.add_argument('gitlaburl', help='URL of the GitLab instance')
parser.add_argument('token', help='Admin API token to read the requested users')
parser.add_argument('--check_period', help='Period in days for which to check activities for.', type=int, default=180)
parser.add_argument('--include_personal_data', help='Include user data in the output files. Default false', action="store_true")
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token.strip())

try:
    users = gl.users.list(iterator=True)
except Exception as e:
    print("Can not retrieve list of users: "+ str(e))
    exit(1)

tz = datetime.now().astimezone().tzinfo

is_bot = lambda account: re.match('(project|group)_.*_bot', account.username)
nonbot_accounts = list(filter(lambda account: not is_bot(account), users))

period = args.check_period

inactive_users = []
no_memberships = []
non_billable_users = []
potential_guest = []
total_users = 0

for user in nonbot_accounts:
    if user.attributes["username"] in ["support-bot","alert-bot","ghost"]:
        continue
    if user.attributes["is_admin"]:
        continue
    if "is_auditor" in user.attributes:
        if user.attributes["is_auditor"]:
            continue
    if user.attributes["state"] != "active":
        continue
    if "current_sign_in_at" not in user.attributes:
        print("Cannot access user field 'current_sign_in_at', use an admin token.")
        exit(1)
    else:
        total_users += 1
        current_sign_in = user.attributes["current_sign_in_at"]
        last_activity = user.attributes["last_activity_on"]
        inactive = True
        if current_sign_in is not None:
            current_sign_in_dt = datetime.strptime(current_sign_in, '%Y-%m-%dT%H:%M:%S.%f%z')
            delta_sign_in = datetime.now(tz) - current_sign_in_dt
            if delta_sign_in.days < period:
                inactive = False
        if last_activity is not None:
            last_activity_dt = datetime.strptime(last_activity, '%Y-%m-%d')
            delta_activity = datetime.now() - last_activity_dt
            if delta_activity.days < period:
                inactive = False
        if inactive:
            inactive_users.append(user.attributes)
            continue
        if not has_memberships(user):
            no_memberships.append(user.attributes)
            continue
        if "using_license_seat" in user.attributes:
            if not user.attributes["using_license_seat"]:
                non_billable_users.append(user.attributes)
                continue
        if check_guest(user, period):
            user_data = user.attributes
            user_data.update(highest_role(gl, user))
            potential_guest.append(user_data)

print("%s total active (not deactivated or blocked) users on the instance" % str(total_users))
print("%s users have NO activity in the last %s days and can be deactivated" % (str(len(inactive_users)), str(period)))
print("%s users are not members of any groups or projects" % str(len(no_memberships)))
print("%s users are non-billable users" % str(len(non_billable_users)))
print("%s users are billable and have no user events above Guest permissions in the last %s days" % (str(len(potential_guest)), str(period)))
print("NOTE: Not all events are tracked, so the list of potential guest users may contain users executing actions that require higher permissions.")
print("For example, environment actions, viewing dashboards, cancelling jobs, managing variables, viewing merge requests etc are all not tracked as events and thus can not be checked by this report.")
print("Please view the potential guest user list as rough estimate, based on those users not pushing, creating or approving merge requests and not destroying environments.")

if not args.include_personal_data:
    reportfilepath = "report/user_counts_%s.txt" % str(datetime.now().date())
    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)
    with open(reportfilepath, "w") as reportfile:
        reportfile.write("Total users: %s\n" % str(total_users))
        reportfile.write("Inactive users: %s\n" % str(len(inactive_users)))
        reportfile.write("Non-billable users: %s\n" % str(len(non_billable_users)))
        reportfile.write("Users with no memberships: %s\n" % str(len(no_memberships)))
        reportfile.write("Potential guest users: %s\n" % str(len(potential_guest)))
else:
    reportfilepath = "report/inactive_users_%s.csv" % str(datetime.now().date())

    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","email","current_sign_in_at","last_activity_on"]
        reportwriter.writerow(fields)
        for user in inactive_users:
            row = []
            for field in fields:
                row.append(user[field])
            reportwriter.writerow(row)

    reportfilepath = "report/no_memberships_users_%s.csv" % str(datetime.now().date())

    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","email","current_sign_in_at","last_activity_on"]
        reportwriter.writerow(fields)
        for user in no_memberships:
            row = []
            for field in fields:
                row.append(user[field])
            reportwriter.writerow(row)

    reportfilepath = "report/non_billable_users_%s.csv" % str(datetime.now().date())

    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","email","current_sign_in_at","last_activity_on"]
        reportwriter.writerow(fields)
        for user in non_billable_users:
            row = []
            for field in fields:
                row.append(user[field])
            reportwriter.writerow(row)

    reportfilepath = "report/potential_guest_users_%s.csv" % str(datetime.now().date())

    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)    
        fields = ["username","name","email","current_sign_in_at","last_activity_on","created_at","highest_access_level_non_personal", "project_maintainer_count_non_personal", "project_owner_count_non_personal", "group_maintainer_count", "group_owner_count", "personal_project_count"]
        reportwriter.writerow(fields)
        for user in potential_guest:
            row = []
            for field in fields:
                row.append(user[field])
            reportwriter.writerow(row)
