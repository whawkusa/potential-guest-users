#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os
import re

def check_guest(user, period):
    user_object = gl.users.list(username=user["username"])[0]
    contribution_events = user_object.events.list(iterator=True, max_retries=15)
    print("%s has been active in the last %s days, checking events." % (user["username"], str(period)))
    for event in contribution_events:
        event_dt = datetime.strptime(event.created_at, '%Y-%m-%dT%H:%M:%S.%f%z')
        delta_event = datetime.now(tz) - event_dt
        if delta_event.days > period:
            print("Parsed all events for %s in the check period and did not find non-guest actions." % user["username"])
            return True
        for action in ["pushed", "accepted", "closed", "approved", "reopened", "destroyed", "merged", "imported"]:
            if action in event.action_name:
                return False
        if event.action_name == "opened" and event.target_type == "MergeRequest":
                return False
    # no events found in period
    print("Parsed all events for %s and did not find non-guest actions." % user["username"])
    return True

parser = argparse.ArgumentParser(description='Identify potential Guest users on GitLab SaaS')
parser.add_argument('token', help='Admin API token to read the requested users')
parser.add_argument('member_csv', help='Group member report CSV file to check')
parser.add_argument('--check_period', help='Period in days for which to check activities for.', type=int, default=180)
parser.add_argument('--include_personal_data', help='Include user data in the output files. Default false', action="store_true")
args = parser.parse_args()

gl = gitlab.Gitlab("https://gitlab.com/", private_token=args.token.strip(), retry_transient_errors=True)

users = {}
with open(args.member_csv,"r") as members_csv:
    reader = csv.reader(members_csv, delimiter=",")
    header = next(reader)
    
    intended_format = ["username","name","email","highest_access_level","last_activity_on","last_login_at","state","using_license_seat","member_of","access_level","type","source"]
    if header == intended_format:
        for row in reader:
            if len(row) > len(intended_format):
                print("Invalid user data for user %s. Too many fields in data." % row[0])
                print("Expecting %s" % intended_format)
                continue
            user = {}
            for idx, field in enumerate(intended_format):
                user[field] = row[idx]
            # representation in the CSV is denormalized, containing multiple lines for users with diverging "member_of", "access_level", "type" and "source" fields.
            # Those fields are irrelevant for our purpose, so we just add the first entry for the user
            if user["username"] not in users:
                users[user["username"]] = user
    else:
        print("CSV format does not equal %s" % intended_format)

users = users.values()

tz = datetime.now().astimezone().tzinfo

is_bot = lambda account: re.match('(project|group)_.*_bot', account["username"])
nonbot_accounts = list(filter(lambda account: not is_bot(account), users))

period = args.check_period

inactive_users = []
private_profile = []
potential_guest = []
total_users = 0

for user in nonbot_accounts:
    total_users += 1
    last_activity = user["last_activity_on"]
    inactive = True
    if "private" in last_activity or not last_activity:
        print("%s has a private profile." % user["username"])
        private_profile.append(user)
    else:
        last_activity_dt = datetime.strptime(last_activity, '%Y-%m-%d')
        delta_activity = datetime.now() - last_activity_dt
        if delta_activity.days >= period:
            print("%s has been inactive in the check_period." % user["username"])
            inactive_users.append(user)
        else:
            if check_guest(user, period):
                potential_guest.append(user)

print("%s total active (non-bot) users on the group" % str(total_users))
print("%s users have NO activity in the last %s days" % (str(len(inactive_users)), str(period)))
print("%s users have a private profile, making it impossible to check their events" % str(len(private_profile)))
print("%s users have NO user events above Guest permissions in the last %s days" % (str(len(potential_guest)), str(period)))
print("NOTE: Not all events are tracked, so the list of potential guest users may contain users executing actions that require higher permissions.")
print("For example, environment actions, viewing dashboards, cancelling jobs, managing variables, viewing merge requests etc are all not tracked as events and thus can not be checked by this report.")
print("Please view the potential guest user list as rough estimate, based on those users not pushing, creating or approving merge requests and not destroying environments.")

if not args.include_personal_data:
    reportfilepath = "report/user_counts_%s.txt" % str(datetime.now().date())
    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)
    with open(reportfilepath, "w") as reportfile:
        reportfile.write("Total users: %s\n" % str(total_users))
        reportfile.write("Inactive users: %s\n" % str(len(inactive_users)))
        reportfile.write("Private profile users: %s\n" % str(len(private_profile)))
        reportfile.write("Potential guest users: %s\n" % str(len(potential_guest)))
else:
    reportfilepath = "report/inactive_users_%s.csv" % str(datetime.now().date())

    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","last_activity_on"]
        reportwriter.writerow(fields)
        for user in inactive_users:
            row = []
            for field in fields:
                row.append(user[field])
            reportwriter.writerow(row)

    reportfilepath = "report/private_profile_users_%s.csv" % str(datetime.now().date())

    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","last_activity_on"]
        reportwriter.writerow(fields)
        for user in private_profile:
            row = []
            for field in fields:
                row.append(user[field])
            reportwriter.writerow(row)

    reportfilepath = "report/potential_guest_users_%s.csv" % str(datetime.now().date())

    os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

    with open(reportfilepath, "w") as reportfile:
        reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
        fields = ["username","name","email","last_activity_on","highest_access_level"]
        reportwriter.writerow(fields)
        for user in potential_guest:
            row = []
            for field in fields:
                row.append(user[field])
            reportwriter.writerow(row)
